# README #

This README would normally document whatever steps are necessary to get your application up and running.

## FoodStore. ##
### Ninja Framework integration example ###

The project is a comprehensive project with database access via JPA, database migrations and advanced features like filters and argument extractors.

### How do I get set up? ###

### Summary of set up ###
* You can import application form eclipse ide by "Import Existing Mavan Project"

* set default jdk instead of jre user jdk 7 or above.


* To run from ide.


```
#!python

  Run Maven Clean.
  Run Maven Install.
  Run -> Run Configuration.
  Create Maven Run Configuration.
    select project directory as base directory.
    Set jetty:run in goal field.
    Select JDK from the java run time environment.
    then apply and run.
 
set goal as jetty:run;

```


To Run Application from the console

```
#!java

cd foodstore
mvn clean install     // to generate the compiled classes the first time
mvn ninja:run         // to start Ninja's SuperDevMode
```


open urk in browser 
http://localhost:8080.

Project Directory Structure


```
#!java

├── pom.xml                                     // Instructions about dependencies and the build (Maven)
└── src
    ├── main
    │   ├── java
    │   │   ├── META-INF
    │   │   │   └── persistence.xml             // Contains informations how to access databases via JPA
    │   │   ├── assets                          // Static assets of your application
    │   │   │   └── css
    │   │   │       └── custom.css
    │   │   ├── conf 
    │   │   │   ├── Module.java                 // Dependency injection definitions via Guice (Optional) 
    │   │   │   ├── Routes.java                 // Contains all routes of your application in one location
    │   │   │   ├── ServletModule.java          // Integration of arbitrary servlet filters and mappings (Optional)
    │   │   │   ├── StartupActions.java         // Customization of application startup (Optional)
    │   │   │   ├── application.conf            // Configuration for test dev and production mode
    │   │   │   ├── messages.properties         // 18n messages
    │   │   │   └── messages_de.properties
    │   │   ├── controllers                     // Controllers will handle the actual request and do something
    │   │   │   ├── ApiController.java
    │   │   │   ├── ApplicationController.java
    │   │   │   ├── FoodController.java
    │   │   │   └── LoginLogoutController.java
    │   │   ├── dao                             // Database access via DAO objects and not in the controller
    │   │   │   ├── FoodDao.java
    │   │   │   ├── SetupDao.java
    │   │   │   └── UserDao.java
    │   │   ├── db                              // Database migrations when dealing with RDBMS (Flyway)
    │   │   │   └── migration
    │   │   │       ├── V1__.sql                // Contains Create table and FK defination.
    │   │   │       └── V2__.sql
    │   │   ├── etc
    │   │   │   ├── LoggedInUser.java
    │   │   │   └── LoggedInUserExtractor.java  // Argument extractors for controller methods
    │   │   ├── filters
    │   │   │   └── LoggerFilter.java           // Filter to filter the request in the controller
    │   │   ├── logback.xml                     // Logging configuration via logback / slf4j
    │   │   ├── models                          // Some models that map to your relational database
    │   │   │   ├── Food.java
    │   │   │   ├── FoodsDto.java
    │   │   │   └── User.java
    │   │   └── views                           // html views - always map to a controller and a method
    │   │       ├── ApplicationController
    │   │       │   ├── index.ftl.html          // Maps to controller "ApplicationController" and method "index"
    │   │       │   └── setup.ftl.html
    │   │       ├── FoodController
    │   │       │   ├── foodShow.ftl.html
    │   │       │   └── placeOrder.ftl.html
    │   │       ├── LoginLogoutController
    │   │       │   ├── login.ftl.html
    │   │       │   └── logout.ftl.html
    │   │       ├── layout
    │   │       │   ├── defaultLayout.ftl.html
    │   │       │   ├── footer.ftl.html
    │   │       │   └── header.ftl.html
    │   │       └── system                      // Error html views. Can be customized to output custom error pages
    │   │           ├── 403forbidden.ftl.html
    │   │           └── 404notFound.ftl.html
    │   ├── resources
    │   └── webapp
    │       └── WEB-INF
    │           └── web.xml                    // Needed for servlet containers to start up Ninja
    └── test
        ├── java
        │   └── controllers                    // Different tests for your application
        │       ├── ApplicationControllerFluentLeniumTest.java
        │       ├── ApplicationControllerTest.java
        │       ├── LoginLogoutControllerTest.java
        │       └── RoutesTest.java
        └── resources
            └── test_for_upload.txt

```

