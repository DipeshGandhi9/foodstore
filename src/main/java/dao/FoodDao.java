package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import models.Food;
import models.FoodsDto;
import ninja.jpa.UnitOfWork;

import com.google.inject.Inject;
import com.google.inject.Provider;

public class FoodDao {
	
	@Inject
    Provider<EntityManager> entitiyManagerProvider;
	
	@UnitOfWork
    public FoodsDto getAllFoods() {
        
        EntityManager entityManager = entitiyManagerProvider.get();
        
        TypedQuery<Food> query= entityManager.createQuery("SELECT x FROM Food x", Food.class);
        List<Food> foods = query.getResultList();        

        FoodsDto foodsDto = new FoodsDto();
        foodsDto.foods = foods;
        
        return foodsDto;
        
    }
	
	@UnitOfWork
    public Food getFood(Long id) {
        
        EntityManager entityManager = entitiyManagerProvider.get();
        
        Query q = entityManager.createQuery("SELECT x FROM Food x WHERE x.id = :idParam");
        Food food = (Food) q.setParameter("idParam", id).getSingleResult();
        
        return food;
        
    }

}
