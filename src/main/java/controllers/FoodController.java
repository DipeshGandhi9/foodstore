package controllers;

import models.Food;
import ninja.Result;
import ninja.Results;
import ninja.params.PathParam;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import dao.FoodDao;

@Singleton
public class FoodController {

	@Inject
	FoodDao foodDao;

	// /////////////////////////////////////////////////////////////////////////
	// Show Food
	// /////////////////////////////////////////////////////////////////////////
	public Result foodShow(@PathParam("id") Long id) {

		Food food = null;

		if (id != null) {

			food = foodDao.getFood(id);

		}

		return Results.html().render("food", food);

	}
	
	// /////////////////////////////////////////////////////////////////////////
	// Place Order
	// /////////////////////////////////////////////////////////////////////////
	public Result placeOrder(@PathParam("id") Long id) {

		Food food = null;

		if (id != null) {

			food = foodDao.getFood(id);

		}

		return Results.html().render("food", food);

	}

}
